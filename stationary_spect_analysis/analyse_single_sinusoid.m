% Demonstration script: Spectral analysis of a single sinusoid using the DFT/FFT
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

fs = 8000;           % Sampling frequency
N  = 64;             % Number of samples to take
%w  = hamming(N);     % Data window: Hamming
w = ones(N,1);       % Data window: Rectangular
f1 = 0.05*fs;        % Frequency of sinusoid to analyse

% Get samples sinusoid
n  = (0:N-1)';
x  = sin(2*pi*(f1/fs)*n);     % N samples of sinusoid with frequency f1 samples at fs
xw = x.*w;                    % Apply data window
Xw = fft(xw);                 % Calculate DFT (FFT)

% Plot time-domain signal
figure(1);
stem(n,xw);
xlabel('Time (samples)');
ylabel('Amplitude');
title('Sampled and windowed single sinusoid');

% Plot spectrum
figure(2);
stem(linspace(0,1-1/N,N),abs(Xw));
xlabel('Frequency (cycles/sample)');
ylabel('Spectral magnitude');
title('Spectrum of single sampled sinusoid');
