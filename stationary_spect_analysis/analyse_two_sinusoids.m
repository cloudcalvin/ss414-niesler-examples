% Demonstration script: Spectral analysis of two sinusoids using the DFT/FFT
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

fs = 8000;          % Sampling frequency
N  = 64;            % Number of samples to take
%w  = hamming(N);    % Data window: Hamming
w = ones(N,1);      % Data window: Rectangular
f1 = 0.10*fs;       % Frequency of sinusoid 1
f2 = 0.125*fs;      % Frequency of sinusoid 2

% Get sampled sum-of-sinusoids
n  = (0:N-1)';
x  = sin(2*pi*(f1/fs)*n) + 0.37*sin(2*pi*(f2/fs)*n); 
xw = x.*w;
Xw = fft(xw);

% Plot time-domain signal
figure(1);
stem(n,xw);
xlabel('Time (samples)');
ylabel('Amplitude');
title('Sampled and windowed sum of two sinusoids');

% Plot spectrum
figure(2);
stem(linspace(0,1-1/N,N),abs(Xw));
xlabel('Frequency (cycles/sample)');
ylabel('Spectral magnitude');
title('Spectrum of sampled sum of two sinusoid');

