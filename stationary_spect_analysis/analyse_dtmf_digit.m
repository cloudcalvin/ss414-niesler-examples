% Demonstration script: Spectral analysis of two sinusoids using the DFT/FFT
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read signal in from file
[x,fs,B] = wavread('dtmf_digit1_16bit_8kHz.wav');

% Determine data window
N  = length(x);
%w  = hamming(N);
w = ones(N,1);

% Depermine spectum of windowed signal
n  = (0:N-1)';
xw = x.*w;
Xw = fft(xw);

% Plot time-domain signal
figure(1);
plot(n(1:200),x(1:200));
xlabel('Time (samples)');
ylabel('Amplitude');
title('Sampled DTMF signal');
legend('off')


% Plot magnitude spectrum
figure(2);
stem(linspace(0,fs*((N-1)/N),N),abs(Xw));
xlabel('Frequency (Hz)');
ylabel('Spectral magnitude');
title('Spectrum of DTMF tone');

% axis([600 1500 0 180])
% Digit1: 697 & 1209 Hz => "1"
% Digit2: 852 & 1477 Hz => "9"
