% Demonstration of upsampling and downsampling.
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

sourcefile      = 'yesterday_lin16_44kHz_mono.wav';
lpfsourcefile   = 'yesterday_lpf.wav';
downsamplefile  = 'yesterday_downsample.wav';
upsamplefile    = 'yesterday_upsample.wav';
lpfupsamplefile = 'yesterday_upsample_lpf.wav';

% Load data from file
[x,fs,nbits]=wavread(sourcefile);

% Plot average spectrum of original signal
figure(1);
get_average_spectrum(x,fs,256,128);
title('Average spectral magnitude: original signal');

% Get LPF filter with cutoff pi/4 rad/sample
[b,a] = butter(16,0.25);
[H,W] = freqz(b,a,1024,'whole');
figure(2);
plot(W/(2*pi),abs(H));
xlabel('Frequency (cycles/sample)');
ylabel('Magnitude response');
title('Downsampling LPF magnitude response')

% Filter signal to avoid aliasing when downsampling
y = filter(b,a,x);
wavwrite(y,fs,nbits,lpfsourcefile);

% Plot average spectrum of LPF signal
figure(3);
get_average_spectrum(y,fs,256,128);
title('Average spectral magnitude: original signal after LPF');

% Downsample by factor of 4:
ydown = downsample(y,4);
wavwrite(ydown,11025,nbits,downsamplefile);

% Plot average spectrum of downsampled signal
figure(4);
get_average_spectrum(ydown,fs,256,128);
title('Average spectral magnitude: downsampled signal');

% Upsample by a factor 4:
yup = upsample(ydown,4);
wavwrite(yup,44100,nbits,upsamplefile);

% Plot average spectrum of upsampled signal
figure(5);
get_average_spectrum(yup,fs,256,128);
title('Average spectral magnitude: upsampled signal');

% Get LPF filter with cutoff pi/4 rad/sample
[b,a] = butter(16,0.25);
[H,W] = freqz(b,a,1024,'whole');
figure(6);
plot(W/(2*pi),abs(H));
xlabel('Frequency (cycles/sample)');
ylabel('Filter magnitude response');
title('Upsampling LPF magnitude response');

% Remove images introduced by upsampling:
ylup = 4*filter(b,a,yup);
wavwrite(ylup,44100,nbits,lpfupsamplefile);

% Plot average spectrum of filtered upsampled signal
figure(7);
get_average_spectrum(ylup,fs,256,128);
title('Average spectral magnitude: upsampled signal after LPF');


