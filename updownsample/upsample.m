% Upsample argument sequence by an integer factor
%
% Usage: y = upsample(x,D)
% 
% Where x is the argument sequence of which will correspond to every Dth sample of y, the rest being zero
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

function y = upsample(x,D)

M = length(x);
y = zeros(M*D,1);

for i = 1:M
 y(1 + (i-1)*D) = x(i);
end
