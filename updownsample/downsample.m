% Downsample argument sequence
%
% Usage: y = downsample(x,D)
% 
% Where x is the argument sequence of which y will contain every Dth sample
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

function y = downsample(x,D)

M = floor(length(x)/D);
y = zeros(M,1);

for i = 1:M
 y(i) = x(1+ (i-1)*D);
end
