% Filter images in the frequency domain
% using a separable rectangular mask
% High-pass filter
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read input image
y = imread('cameraman.png');

% Display original image
figure(1);
imshow(y);
title('Original image');

% Get dimensions of input image
[N1,N2] = size(y)

% Calculate 2D FFT of image
Y = fft2(y);

% Display FFT mgnitude
figure(2);
mesh(abs(Y(1:4:end,1:4:end)))
zlim([0 1e5])
colormap(winter);
title('Magnitudes of 2D FFT');
xlabel('k_1');
ylabel('k_2');

% Prepare filter as mask in the frequency domain
fwc1 = 0.1;   % Cutoff frequency for fw1
fwc2 = 0.1;   % Cutoff frequency for fw2

Nc1 = round(fwc1*N1)+1   % Cutoff in terms of k1
Nc2 = round(fwc2*N2)+1   % Cutoff in terms of k2

M = zeros(N1,N2);            % Initialise mask

% Populate corner near (0,0) with mask values 
M(1:end,Nc2:end) = 1;
M(Nc2:end,1:end) = 1;

% Reflect vertically
for n1 = N1:-1:N1-floor(N1/2)
 M(n1,:) = M(N1 - n1 +1,:);
endfor

% Reflect horizontally
for n2 = N2:-1:N2-floor(N2/2)
 M(:,n2) = M(:,N2-n2+1);
endfor

% Done preparing mask, now display it
figure(3)
imshow(M,[-1 2]);
title('Low-pass filter Fourier amplitudes')
figure(4)
title('Low-pass filter Fourier amplitudes')
mesh(0:4:N1-1,0:4:N2-1,M(1:4:N1,1:4:N2)); % plot only every 4th point else mesh is too dense
axis([0 N1-1 0 N2-1 -0.2 1.2])
colormap(winter); % bone, cool, copper, hot, autumn, hsv, jet, ocean, prism, rainbow, summer, winter, gray
str = ['2D rectangular mask']; 
title(str,"fontsize",25);
xlabel("k_1","fontsize",25);
ylabel("k_2","fontsize",25);
zlabel("M[k_1,k_2]","fontsize",25);
grid("on");

% Wait for input
% --------------
input("Hit <return> to continue ...");


% Apply mask: point-by-point multiplication in frequency domain
Yf = Y.*M;

% Apply 2D IFFT, take real part to eliminate small imaginary
% values due to numerical inaccuracies in the FFT/IFFT implementation
yf = real(ifft2(Yf));

% Display resulting spectrum
figure(5);
mesh(abs(Yf(1:4:end,1:4:end)))
zlim([0 1e5])
colormap(winter);
title('FFT magnitudes after masking');
xlabel('k_1');
ylabel('k_2');

% Display resulting image
figure(6);
imshow(yf,[0 256]);
title('Filtered image');

imwrite(yf,"testout.png");

