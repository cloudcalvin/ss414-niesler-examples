% Filter images in the frequency domain
% using a nonseparable Gaussian mask
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read input image
y = imread('cameraman.png'); 

% Display original image
figure(1);
imshow(y);
title('Original image');

% Get dimensions of input image
[N1,N2] = size(y)

% Calculate 2D FFT of image
Y = fft2(y);

% Display FFT mgnitude
figure(2);
mesh(abs(Y(1:4:end,1:4:end)))
zlim([0 1e5])
colormap(winter);
title('Magnitudes of 2D FFT');
xlabel('k_1');
ylabel('k_2');

% Prepare filter as mask in the frequency domain
sigma = 2;           % Standard deviation, in samples
M = zeros(N1,N2);    % Initialise mask

% populate corner near (0,0) with mask values 
for n1 = 1:floor(N1/2)
 for n2 = 1:floor(N2/1)
   M(n1,n2) = exp(-(n1^2+n2^2)/(2*sigma)^2);
 endfor
endfor

% Reflect vertically
for n1 = N1:-1:N1-floor(N1/2)
 M(n1,:) = M(N1 - n1 +1,:);
endfor

% Reflect horizontally
for n2 = N2:-1:N2-floor(N2/2)
 M(:,n2) = M(:,N2-n2+1);
endfor

% Done preparing mask, now display it
figure(3)
imshow(M,[-1 2]);
title('Low-pass filter Fourier amplitudes')
figure(4)
title('Low-pass filter Fourier amplitudes')
mesh(0:4:N1-1,0:4:N2-1,M(1:4:N1,1:4:N2)); % plot only every 4th point else mesh is too dense
axis([0 N1-1 0 N2-1 -0.2 1.2])
colormap(winter); % bone, cool, copper, hot, autumn, hsv, jet, ocean, prism, rainbow, summer, winter, gray
str = ['2D Gaussian mask, N_1=',num2str(N1),'N_2=',num2str(N2),'\sigma=',num2str(sigma)]; 
title(str,"fontsize",25);
xlabel("k_1","fontsize",25);
ylabel("k_2","fontsize",25);
zlabel("M[k_1,k_2]","fontsize",25);
grid("on");

% Wait for input
% --------------
input("Hit <return> to continue ...");

% Apply mask: point-by-point multiplication in frequency domain
Yf = Y.*M;

% Apply 2D IFFT, take real part to eliminate small imaginary
% values due to numerical inaccuracies in the FFT/IFFT implementation
yf = real(ifft2(Yf));

% Display resulting spectrum
figure(5);
mesh(abs(Yf(1:4:end,1:4:end)))
zlim([0 1e5])
colormap(winter);
title('FFT magnitudes after masking');
xlabel('k_1');
ylabel('k_2');

% Display resulting image
figure(6);
imshow(yf,[1 256]);
title('Filtered image');

% Display IFFT of mask (no sidelobes - Gaussian)
figure(7);
m = ifft2(M);
mesh(real(m(1:2:end,1:2:end)))
%zlim([-0.0005 0.0005])
colormap(winter);
zlim([-0.005 0.005])
title('2D IFFT of mask');
xlabel('n_1');
ylabel('n_2');

imwrite(yf,"testout.png");
