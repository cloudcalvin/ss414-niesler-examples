% Create image of 2D sinusoid
% Set x1 and x2 frequencies of sinusoids independently
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

f1 = 4;
f2 = 0;
fs = 40;
T = 1/fs;

% For display purposes only
fw1 = f1/fs
fw2 = f2/fs

t1min = 0;
t2min = 0;
t1max = 1;
t2max = 1;
 
t1 = t1min:T:t1max-T;
t2 = t2min:T:t2max-T;

[t1grid t2grid] = meshgrid(t1,t2);

y = cos(2*pi*(f1*t1grid + f2*t2grid));

figure(1);
mesh(t1,t2,y);
axis([t1min t1max t2min t2max -2 2])
colormap(winter); % bone, cool, copper, hot, autumn, hsv, jet, ocean, prism, rainbow, summer, winter, gray
if (f1 == 0) 
 str = ['2D cos: y = cos(2\pi f_{\omega2} x_2)  where  f_{\omega2} = f_2/f_s, f_2 = ',num2str(f2),' Hz, f_s = ',num2str(fs), 'Hz']; 
elseif (f2 == 0)
 str = ['2D cos: y = cos(2\pi f_{\omega1} x_1)  where  f_{\omega1} = f_1/f_s, f_1 = ',num2str(f1),' Hz, f_s = ',num2str(fs), 'Hz']; 
else
  str = ['2D cos: y = cos(2\pi f_{\omega1} x_1 + 2\pi f_{\omega2} x_2)  where  f_{\omega1} = f_1/f_s, f_{\omega2} = f_2/f_s, f_1 = ',num2str(f1),'Hz, f_2 = ',num2str(f2),'Hz, f_s = ',num2str(fs), 'Hz']; 
endif
title(str,"fontsize",25);
xlabel("x_1","fontsize",25);
ylabel("x_2","fontsize",25);
zlabel("y(x_1,x_2)","fontsize",25);
grid("on");


if (f1 == 0) 
 print("2dcos_x2.eps","-depsc");
elseif (f2 == 0)
 print("2dcos_x1.eps","-depsc");
else
 print("2dcos_x1x2.eps","-depsc");
endif

n1max = length(t1)-1;
n2max = length(t2)-1;

n1 = 0:n1max;
n2 = 0:n2max;

Y = fft2(y);
figure(2);
mesh(n1/(n1max+1),n2/(n2max+1),abs(Y));
%axis([0 n1max 0 n2max -2 2])
colormap(winter); % bone, cool, copper, hot, autumn, hsv, jet, ocean, prism, rainbow, summer, winter, gray

xlabel("f_{\omega1}");
ylabel("f_{\omega2}");
zlabel("|X[k_1,k_2]|");

