% Create image of 2D sinusoid
% Set x1 and x2 frequencies by cos(theta)
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

f = 4;
theta = 0*pi/2;
f1 = f*cos(theta)
f2 = f*sin(theta)

% For display purposes only
fw1 = f1/fs
fw2 = f2/fs

t1max = 1;
t2max = 1;
fs = 20;
T = 1/fs;

t1 = -t1max+T:T:t1max;
t2 = -t2max+T:T:t2max;

disp(['Inpute data is ',num2str(length(t1)),'x',num2str(length(t2))])

[t1grid t2grid] = meshgrid(t1,t2);

y = cos(2*pi*(f1*t1grid + f2*t2grid));

%% Apply data window 
w1 = hamming(length(t1));
w2 = hamming(length(t2));
w  = w1 * w2';
% y = y .* w;

figure(1);
mesh(t1,t2,y);
axis([-t1max t1max -t2max t2max -2 2])
colormap(cool); % bone, cool, copper, hot, autumn, hsv, jet, ocean, prism, rainbow, summer, winter, gray
xlabel("t1");
ylabel("t2");
zlabel("y");
grid("on");

n1max = length(t1)-1;
n2max = length(t2)-1;

n1 = 0:n1max;
n2 = 0:n2max;

Y = fft2(y);
figure(2);
mesh(n1/(n1max+1),n2/(n2max+1),abs(Y));
%axis([0 n1max 0 n2max -2 2])
colormap(winter); % bone, cool, copper, hot, autumn, hsv, jet, ocean, prism, rainbow, summer, winter, gray
xlabel("f_{\omega1}");
ylabel("f_{\omega2}");
zlabel("|Y|");

