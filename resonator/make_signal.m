% Generate a pulsed sinusoid buried in noise.
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

fs = 44100;   % Sample frequency
fx = 500;     % Frequency of sinusoid;
Ax = 1.0;     % Amplitude of sinusoid;
An = 2.25;    % Standard deviation of noise
Tx = 0.25;    % Length of sinusoid pulse
Tn = 0.25;    % Length of sinusoid silence
Np = 8;       % Number of sinusoid pulses to generate

fw = fx/fs;                      % Freq in rad/sample
Nx = round(Tx*fs);               % Number of samples in sine portion
Nn = round(Tn*fs);               % Number of samples in noise portion
x  = Ax*sin(2*pi*fw*(0:Nx-1));   % Sinusoid burst

SNR = 10*log10(Ax*Ax/2) - 10*log10(An*An)

N = Np*(Nx+Nn) + Nn;             % Total samples
s = An*randn(1,N);               % Gaussian noise with std dev An

% Create signal
for i = 1:Np
 j = (i-1)*(Nx+Nn)+Nn;
 s(j:j+Nx-1) = s(j:j+Nx-1) + x;
end

% Save to WAV file
wavwrite(s'/max(abs(s)),fs,16,'pulsed_sine.wav');

