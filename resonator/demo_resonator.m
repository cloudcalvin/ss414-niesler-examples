% Detect pulsed sinusoid in noise
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

srcfn = 'pulsed_sine.wav';

% Read signal from file
[x,fs,B] = wavread(srcfn);

% Resonator parameters
r  = 0.999;  % Resonator pole radius
f0 = 500;    % Resonator frequency (Hz)

% Transfer function calculation
fw0 = f0/fs;     % Resonator frequency (cycles/sample)
w0  = 2*pi*fw0;  % Resonator frequency (rad/sample)
b0  = 1/(2*sin(w0)/((1-r)*sqrt(1-2*r*cos(2*w0)+r*r)));
b   = [b0 0 -b0];
a   = [1 -2*r*cos(w0) r*r];


% Plot resonator frequency response
figure(1);
[H,W]=freqz(b,a,10240,'whole');
f = ((0:length(W)-1)*fs)/length(W);
plot(f,20*log10(abs(H)));
axis([0 3000 -50 5]);
xlabel('Frequency (Hz)')
ylabel('Gain (dB)');
title('Resonator frequeny response');
grid('on');

% Filter signal with resonator:
y = filter(b,a,x);

% Plot extract of unfiltered signal
figure(2);
plot(x(1000:2299));
xlabel('Time (samples)');
ylabel('Amplitude');
title('Unfiltered signal (extract)');
axis('auto');

% Plot extract of filtered signal
figure(3);
plot(y(1:79999));
xlabel('Time (samples)');
ylabel('Amplitude');
title('Filtered signal (extract)');
axis('auto');

% Save filtered signal to WAV file
wavwrite(y/max(abs(y)),fs,B,'filtered_signal.wav');

