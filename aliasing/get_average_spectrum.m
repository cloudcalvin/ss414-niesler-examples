% Determine and plot the average spectrum of a nonstationary signal.
%
% Usage: y = get_average_spectrum(x,fs,framelength,frameskip)
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

function y = get_average_spectrum(x,fs,framelength,frameskip)

% Divide input signal into frames
F = getframes(x,framelength,frameskip);
[framelength,nframes] = size(F);

% Accumulated FFTs of windowed frames
S = zeros(framelength,1);
w = hamming(framelength);
for i = 1:nframes
 S += fft(F(:,i).*w);
end

% Normalise by number of accumulations
S = S / nframes;

% Plot
plot(linspace(0,fs,framelength),abs(S));
xlabel('Frequency (Hz)')
ylabel('Spectral magnitude');
title('Average spectral magnitude');

% Return average spectrum
y = S;
