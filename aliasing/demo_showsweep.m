% Demonstration script: Display first few samples of the sweep signal
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2014.

% Read audio from file
[x,fs,B] = wavread('sweep_44kHz.wav');

% Plot signal
figure(1);
plot(x(1:2000));
title('First few samples of sweep signal');
xlabel('Samples')
ylabel('Amplitude')
legend('off')
