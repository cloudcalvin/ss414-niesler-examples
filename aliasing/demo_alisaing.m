% Demonstration script: Alisasing when reducing the sample rate
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read audio from file
[x,fs,B] = wavread('yesterday_lin16_44kHz_mono.wav');

% Get average spectrum for original signal
figure(1)
y = get_average_spectrum(x,fs,256,128);
title("Average spectrum at original sample rate (44kHz)");
print("average_spectrum_44kHz.ps","-dpsc")

% Simulate sampling at half the sample rate (22kHz)
x2 = downsample(x,2);
figure(2);
y2 = get_average_spectrum(x2,fs/2,256,128);
title("Average spectrum sampling at at 22kHz");
print("average_spectrum_22kHz.ps","-dpsc")
wavwrite(x2,22000,B,"yesterday_lin16_22kHz_mono.wav");

% Simulate sampling at a quarter of the sample rate
x4 = downsample(x,4);
figure(3);
y4 = get_average_spectrum(x4,fs/4,256,128);
title("Average spectrum sampling at at 11kHz");
print("average_spectrum_11kHz.ps","-dpsc")
wavwrite(x4,11000,B,"yesterday_lin16_11kHz_mono.wav");


% Simulate sampling at an eighth of the sample rate
x8 = downsample(x,8);
figure(4);
y8 = get_average_spectrum(x8,fs/8,256,128);
title("Average spectrum sampling at at 5.5kHz");
print("average_spectrum_5.5kHz.ps","-dpsc")
wavwrite(x8,5500,B,"yesterday_lin16_5.5kHz_mono.wav");


