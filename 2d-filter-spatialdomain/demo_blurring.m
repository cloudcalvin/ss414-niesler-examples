% Filter images in the time domain
% using square blurring kernel
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read input image
y = imread('cameraman.png'); 

% Display original image
figure(1);
imshow(y);
title('Original image');

% Get dimensions of input image
[N1,N2] = size(y);

% Set up blurring 2D signals
M = 35;
h = ones(M,M)/(M*M);

 
% Apply blurring kernel detectors
y = conv2(y,h);

% Display resulting images
figure(2);
imshow(y,[0 256]);
str = ['Blurred image using ',num2str(M),'x',num2str(M),' square kernel'];
title(str);







 
