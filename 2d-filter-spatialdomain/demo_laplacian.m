% Filter images in the time domain
% using Laplacian edge detection kernel
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read input image
y = imread('cameraman.png'); 

% Display original image
figure(1);
imshow(y);
title('Original image');

% Get dimensions of input image
[N1,N2] = size(y);

% Laplacian mask
h_laplace1 = [0  1 0 ; 
              1 -4 1 ; 
              0  1 0];

h_laplace2 = [1  1 1 ; 
              1 -8 1 ; 
              1  1 1];

 
% Apply edge detectors
y1 = conv2(y,h_laplace1);
y2 = conv2(y,h_laplace2);

% Display resulting images
figure(2);
imshow(y1,[0 256]);
title('Laplacian edge detection: isotropic \pi/2');

figure(3);
imshow(y2,[0 256]);
title('Laplacian edge detection: isotropic \pi/4');





 
