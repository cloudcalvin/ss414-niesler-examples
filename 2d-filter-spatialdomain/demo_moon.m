% Filter images in the time domain
% using Sobel edge detection kernel
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read input image
y = imread('moon_north_pole.png');

% Display original image
figure(1);
imshow(y);
title('Original image');

% Get dimensions of input image
[N1,N2] = size(y);
 
h_laplace = [1  1 1 ; 
             1 -8 1 ; 
             1  1 1];

y_laplace = conv2(y,h_laplace);

% Display resulting images
figure(2);
imshow(y_laplace,[0 64]);
title('Laplace filtered image');

[n1,n2] = size(y_laplace);
dy = y_laplace(2:n1-1,2:n2-1);

figure(5)
imshow(0.3*(abs(dy)+100)+y)
title('Superposition of original and Laplace filtered output');





 
