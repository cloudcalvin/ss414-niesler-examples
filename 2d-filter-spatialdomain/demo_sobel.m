% Filter images in the time domain
% using Sobel edge detection kernel
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read input image
y = imread('cameraman.png'); 

% Display original image
figure(1);
imshow(y);
title('Original image');

% Get dimensions of input image
[N1,N2] = size(y);

% Set up edge detection 2D signals
% Horizontal Sobel mask
h_horiz = [-1 0 1 ; 
           -2 0 2 ; 
           -1 0 1];

% V ertcal Sobel mask
h_vert = [-1 -2 -1;
           0  0  0;
           1  2  1];
 
 
% Apply edge detectors
y_horiz = abs(conv2(y,h_horiz));
y_vert  = abs(conv2(y,h_vert));

% Display resulting images
figure(2);
imshow(y_horiz,[0 256]);
title('Horizontal edge detection');

figure(3);
imshow(y_vert,[0 256]);
title('Vertical edge detection');

y_all = y_vert + y_horiz;
figure(4)
imshow(y_all,[0 256]);
title('Combined edge detection');





 
