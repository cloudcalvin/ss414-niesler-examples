% Speech enhancement in the presence of turbine noise
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

srcfn = 'speech-in-turbine-noise1.wav';

% Read signal from file
[x,fs,B] = wavread(srcfn);

% Plot signal spectrum
figure(1);
X = fft(x.*hamming(length(x)));
f = ((0:length(x)-1)/length(x))*fs;
plot(f,log10(abs(X)));
xlabel('Frequency (Hz)')
ylabel('Log magnitude');
title('Unfiltered signal');
axis("auto");

% Prototype: notch parameters
r  = 0.6;     % Notch pole radius
f0 = 0;       % Notch frequency (Hz)

% Comb parameters
L = 14;       % Notches at 44100/14 = 3150Hz

% Transfer function calculation
fw0 = f0/fs;     % Notch frequency (cycles/sample)
w0  = 2*pi*fw0;  % Notch frequency (rad/sample)
b   = [1 zeros(1,L-1) -2*cos(w0)   zeros(1,L-1) 1];
a   = [1 zeros(1,L-1) -2*r*cos(w0) zeros(1,L-1) r*r];

% Plot notch frequency response
figure(2);
[H,W]=freqz(b,a,10240,"whole");
f = ((0:length(W)-1)*fs)/length(W);
plot(f,20*log10(abs(H)));
xlabel('Frequency (Hz)')
ylabel('Gain (dB)');
title('Notch frequeny response');
grid("on");
axis([0 44100 -10 5]);

% Filter signal with resonator:
y = filter(b,a,x);

% Plot filtered signal spectrum
figure(3);
Y = fft(y.*hamming(length(y)));
f = ((0:length(y)-1)/length(y))*fs;
plot(f,log10(abs(Y)));
xlabel('Frequency (Hz)')
ylabel('Log magnitude');
title('Filtered signal');
axis("auto");

% Save filtered signal to WAV file
wavwrite(y/max(abs(y)),fs,B,"filtered_signal.wav");

