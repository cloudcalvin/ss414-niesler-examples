%% Systems and Signals 414: resolving closely-spaces sinusoids
%% Thomas Niesler, Stellenbosch University, DSP group, 2007-2014


N = 100;        % Number of samples 
Nzp = 5000-N;   % Number to zero-pad with (for improved display resolutions)

Bw = 4/N;     % Main lobe width. 4/N for Hamming, 6/N for Blackmann  

fw1 = 0.2;      % Frequency of first component
fw2 = fw1 + Bw; % Frequency of second component

n = 0:N-1;
y1 = cos(2*pi*fw1*n);  % First component
y2 = cos(2*pi*fw2*n);  % Second component


% Choose window here
w = hamming(N)';
%w = blackman(N)';

% Apply window
y1 = y1.*w;
y2 = y2.*w;

% Zero-pad
y1zp = [y1 zeros(1,Nzp)];
y2zp = [y2 zeros(1,Nzp)];

% Add two components together
yzp = y1zp + y2zp;

% Get FFT
Yzp = fft(yzp);

% Normalise so peak is at 0dB
Yzp = Yzp/max(Yzp);

% x-axis frequencies (cycles/sample)
fw = (0:N+Nzp-1)/(N+Nzp);

% Plot spectrum
plot(fw,20*log10(abs(Yzp)))
xlabel('f_w (cylces/sample)');
ylabel('Log spectral magnitude (dB)')

% Set graph axis ranges
ymin = -80;
ymax = 40;

% Plot lines to show two components
hold on
plot([fw1 fw1],[ymin ymax],'r-')
plot([fw2 fw2],[ymin ymax],'r-')
hold off

% Wait for input
% --------------
input("Hit <return> to continue ...");

axis([0.1 0.3 ymin ymax])
grid on

% Wait for input
% --------------
input("Hit <return> to continue ...");


% The following also superimposes the FFT's of the two individual components
hold on
Y1zp = fft(y1zp);
Y1zp = Y1zp/max(Y1zp);
Y2zp = fft(y2zp);
Y2zp = Y2zp/max(Y2zp);
plot(fw,20*log10(abs(Y1zp)),'g')
plot(fw,20*log10(abs(Y2zp)),'m')
hold off
