% Introduce two echos into a signal
%
% Usage: y = doubleecho(x,delay1,attenuation1,delay2,attenuation2)
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

function y = doubleecho(x,delay1,attenuation1,delay2,attenuation2)

h = zeros(1,max([delay1 delay2]));
h(1) = 1;
h(delay1) = attenuation1;
h(delay2) = attenuation2;

y = filter(h,[1],[x ; zeros(length(h),1)]);

