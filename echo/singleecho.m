% Introduce a single echo into a signal
%
% Usage: [y,h] = singleecho(x,delay,attenuation)
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

function [y,h] = singleecho(x,delay,attenuation)

h = [1 zeros(1,delay-1) attenuation];
y = filter(h,[1],[x ; zeros(delay,1)]);

