% Demonstration script: Adding multiple echoes to a signal
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read audio from file
[x,fs,B]=wavread('where_were_you-male.wav');

% Introduce echos of various lengths and save as audio.

attenuation = 0.9;
numechoes   = 8;

disp('Processing 500-sample echo ...');
[y,h1] = multipleecho(x,500,attenuation,numechoes);
wavwrite(y,fs,B,'result_multipleecho_500x8.wav')

disp("Processing 5000-sample echo ...");
[y,h2] =multipleecho(x,5000,attenuation,numechoes);
wavwrite(y,fs,B,'result_multipleecho_5000x8.wav')

% Done.

