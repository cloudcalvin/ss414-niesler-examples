% Demonstration script: Filtering a signal using a reverberant room's impulse response
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read speech audio from file
[x,fs,B]=wavread('where_were_you-male.wav');

% Read impulse response from file
[h,fs,B]=wavread('clap-in-reverberant-room.wav');

% Filter speech with impulse response
y = filter(h,[1],[x ; zeros(length(h),1)]);

% Save to file
wavwrite(y/max(abs(y)),fs,B,'result_reverberation.wav')

% Done.

