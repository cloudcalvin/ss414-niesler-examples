% Introduce <N> echoes into a signal at multiples of <delay> and with decay constant <attenuation>
%
% Usage: [y,h] = multipleecho(x,delay,attenuation,N)
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

function [y,h] = multipleecho(x,delay,attenuation,N)

if (N < 1) 
 disp('*** ERROR: must introduce at least one echo!')
 return;
endif

% Set up impulse reponse:
h = zeros(1,N*delay);
h(1) = 1;
a = attenuation;
for i=1:N
 h(i*delay) = a;
 a = a * attenuation;
end

% Filter zero-padded signal:
y = filter(h,[1],[x ; zeros(length(h),1)]);

