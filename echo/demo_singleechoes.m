% Demonstration script: Adding single echoes to a signal
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read audio from file
[x,fs,B]=wavread('where_were_you-male.wav');

% Introduce echos of various lengths and save as audio.

attenuation = 0.7;

disp('Processing 100-sample echo ...');
y=singleecho(x,100,attenuation);
wavwrite(y,fs,B,'result_singleecho_100.wav')

disp('Processing 500-sample echo ...');
y=singleecho(x,500,attenuation);
wavwrite(y,fs,B,'result_singleecho_500.wav')

disp('Processing 1000-sample echo ...');
y=singleecho(x,1000,attenuation);
wavwrite(y,fs,B,'result_singleecho_1000.wav')

disp('Processing 2500-sample echo ...');
y=singleecho(x,2500,attenuation);
wavwrite(y,fs,B,'result_singleecho_2500.wav')

disp('Processing 5000-sample echo ...');
y=singleecho(x,5000,attenuation);
wavwrite(y,fs,B,'result_singleecho_5000.wav')

disp('Processing 7500-sample echo ...');
y=singleecho(x,7500,attenuation);
wavwrite(y,fs,B,'result_singleecho_7500.wav')

% Done.

