% Divide data vector x into frames.
%
% F = getframes(x,framelength,frameskip)
%
% F will be Nf by Lf, where NF is the number of frames and Lf the frame length
% 
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.
%
function F = getframes(x,framelength,frameskip)

[r,c] = size(x);

if ((r != 1) && (c != 1)) then
 disp('ERROR: x should be a vector');
 return;
endif

% Make sure x is a column vector
if (c > 1)
 x = x';
endif

Lx = length(x);

if (Lx < framelength) then
 disp('ERROR: x is shorter than even a single frame');
 return;
endif

% Determine number of frames that can be extracted from data vector x.
% "fix" takes integer part, like "trunc" in matlab
nFrames = fix((Lx-framelength)/frameskip + 1)

% Prepare output matrix
F = zeros(framelength,nFrames);

% Now divide x into frames
for i = 1:nFrames
 xIndex = (i-1)*frameskip + 1;
 F(:,i) = x(xIndex:xIndex+framelength-1);
end

% --- DONE ---

