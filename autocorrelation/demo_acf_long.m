% Demonstration script: Analysis of singing voice using autocorrelation
% Case 2: several notes (nonstationary signal)
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

framelength = 2048; 
frameskip   = 64;
maxlag      = 200;

% Read audio from file
[x,fs,B] = wavread("singing-voice.wav");

% Divide signal into frames
F = getframes(x,framelength,frameskip);

% Get number of frames
[a,nframes] = size(F);

% Prepare matrix to hold sequence of ACF vectors
R = zeros(maxlag+1,nframes);

for i = 1:nframes
 r = xcorr(F(:,i),F(:,i),maxlag);    % Get ACF
 R(:,i) = r(maxlag+1:2*maxlag+1);    % Paste into matrix R
 R(:,i) = R(:,i)/R(1,i);             % Normalise by r_xx[0]
end


% Plot as image
colormap(ocean(256));      
imagesc(flipud(R),[0 1]);   % Flip to let vert axis increase going up
