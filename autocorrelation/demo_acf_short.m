% Demonstration script: Analysis of singing voice using autocorrelation
% Case 1: single note (stationary signal)
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

maxlag = 199;  % Will calculate r_xx[0 ... maxlag]

% Read audio from file
[x,fs,B] = wavread("singing-voice-single-note.wav");

% Determine autocorrelation
r = xcorr(x,x,maxlag);

% Remember that autocorrelation is symmetrical about l == 0
% Extract r_xx[0 ... maxlag]
R = r(maxlag+1:2*maxlag+1);  

% Normalise so that r_xx[0] == 1
R = R/R(1);

% Plot
plot(R)
xlabel("Lag (samples)")
ylabel("Normalised ACF")
title('Autocorrelation of single note sung by soprano')
grid('on')
legend('off')

% Now calclate pitch: fo = fs/lmax
% Where lmax is lag at which ACF peak occurs
% Peak at lmax = 95, so fo = 44100/95 = 464.2 Hz
% Check: Measure time for 10 periods ~= 0.021s, hence fo ~=476.2 Hz
