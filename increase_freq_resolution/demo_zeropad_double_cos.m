% Demonstration script: Zero-padding to increase frequency resolution
% Case 2: two sinusoids
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

f1   = 35/20;  % Frequency of first continuous-time sinusoid (Hz)
f2   = 38/20;  % Frequency of second continuous-time sinusoid (Hz)
a1   = 1;      % Amplitude of first sinusoid
a2   = 0.8;    % Amplitude of second sinusoid
fs   = 10;     % Sampling frequency (Hz)
N    = 80;     % Number of samples to take 
N_zp = 720;    % Number of samples to zero pad with 


n = 0:N-1;                                          % Discrete-time
t = n/fs;                                           % Sampling instants
x = a1*cos(2*pi*f1*n/fs) + a2*cos(2*pi*f2*n/fs);    % Signal samples
%w = ones(N,1);                                     % Get data window (Rectangular)
w = hamming(N);                                     % Get data window (Hamming)
xw = x.*w';                                         % Apply data window
X = fft(xw);                                        % Spectrum

% Plot spectrum for unpadded case
% -------------------------------

figure(1);
f = fs*(n/N);
stem(f,abs(X));
xlabel('Frequency (Hz)');
ylabel('Spectral magnitude');
title('Sum of two sinusoids, no zero-padding');
axis([min(f)-1 max(f)+1 min(abs(X))-0.2 max(abs(X))+0.2])
grid('on');

% Zero-pad x and get its spectrum
% -------------------------------

n_zp = 0:N+N_zp-1;         % Discrete-time
x_zp = [xw zeros(1,N_zp)];  % Zero-paddes signal
X_zp = fft(x_zp);


% Plot spectrum for unpadded case
% -------------------------------

figure(2);
f = fs*(n_zp/(N+N_zp));
stem(f,abs(X_zp));
xlabel('Frequency (Hz)');
ylabel('Spectral magnitude');
str = ['Sum of two sinusoids, padded by ',num2str(N_zp),' to length ',num2str(N+N_zp)];
title(str);
axis([min(f)-1 max(f)+1 min(abs(X_zp))-0.2 max(abs(X_zp))+0.2])
grid('on');

% Zoom in: axis([1.3 2.3 0 35])
% Peaks at approx 1.75 and 1.9 Hz




