% Demonstration script: Zero-padding to increase frequency resolution
% Case 1: single sinusoid
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

f    = 1;     % Frequency of continuous-time signal
fs   = 10;    % Sampling frequency
N    = 10;    % Number of samples to take
N_zp = 40;    % Number of samples to zero pad with 


n = 0:N-1;  % Discrete-time
t = n/fs;   % Sampling instants
x = cos(2*pi*f*n/fs);
X = fft(x);

% Plot time and frequency-domain representations
% ----------------------------------------------
figure(1);
stem(n,x);
xlabel('Time (samples)');
ylabel('Amplitude');
title('Single cycle of a sinusoid, no zero-padding');
axis([min(n)-0.2 max(n)+1 min(x)-0.2 max(x)+0.2])
grid('on');

figure(2);
f = fs*(n/N);
stem(f,abs(X));
xlabel('Frequency (Hz)');
ylabel('Spectral magnitude');
title('Spectrum of a single cycle of a sinusoid, no zero-padding');
axis([min(f)-1 max(f)+1 min(abs(X))-0.2 max(abs(X))+0.2])
grid('on');



% Zero-pad x and get its spectrum
% -------------------------------

n_zp = 0:N+N_zp-1;         % Discrete-time
x_zp = [x zeros(1,N_zp)];  % Zero-paddes signal
X_zp = fft(x_zp);


% Plot time and frequency-domain representations of zero-padded signal
% --------------------------------------------------------------------


figure(3);
stem(n_zp,x_zp);
xlabel('Time (samples)');
ylabel('Amplitude');
str = ['Single cycle of a sinusoid, padded by ',num2str(N_zp),' to length ',num2str(N+N_zp)];
title(str);
axis([min(n_zp)-1 max(n_zp)+1 min(x_zp)-0.2 max(x_zp)+0.2])
grid('on');


figure(4);
f = fs*(n_zp/(N+N_zp));
stem(f,abs(X_zp));
xlabel('Frequency (Hz)');
ylabel('Spectral magnitude');
str = ['Spectrum of a single cycle of a sinusoid, padded by ',num2str(N_zp),' to length ',num2str(N+N_zp)];
title(str);
axis([min(f)-1 max(f)+1 min(abs(X_zp))-0.2 max(abs(X_zp))+0.2])
grid('on');


% Zoom: axis([1.4 2.8 0 6])
% Peak at 2.2


