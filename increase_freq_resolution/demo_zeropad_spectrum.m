% Demonstration script: Zero-padding to increase time resolution
% Case 1: single sinusoid
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

f    = 4;     % Frequency of continuous-time signal
fs   = 10;    % Sampling frequency
N    = 10;    % Number of samples to take
N_zp = 30;     % Number of samples to zero pad with 


n = 0:N-1;  % Discrete-time
t = n/fs;   % Sampling instants
x = cos(2*pi*f*n/fs);
X = fft(x);

% Plot time and frequency-domain representations
% ----------------------------------------------
figure(1);
stem(n,x);
xlabel('Time (samples)');
ylabel('Amplitude');
title('Samples of sinusoidal signal');
axis([min(n)-0.2 max(n)+1 min(x)-0.2 max(x)+0.2])
grid('on');

% Wait for input and then show continuous-time envelope
% -----------------------------------------------------
input("Hit <return> to continue ...");
nc = 0:0.02:N-1;    % More closely-spaced samples
xc = cos(2*pi*f*nc/fs);
hold('on');
plot(nc,xc,'b--');
hold('off');


% Wait for input and then plot spectrum
% -------------------------------------
input("Hit <return> to continue ...");

figure(2);
f = fs*(n/N);
stem(f,abs(X));
xlabel('Frequency (Hz)');
ylabel('Spectral magnitude');
title('Spectrum of sinusoid');
axis([min(f)-1 max(f)+1 min(abs(X))-0.8 max(abs(X))+0.8])
grid('on');


% Get spectrum and then zero pad
% -------------------------------

input("Hit <return> to continue ...");

X = fft(x);
n_zp = 0:N+N_zp-1;         % Discrete-time
X_zp = [X(1:(N/2)) zeros(1,N_zp) X((N/2)+1:N)];  % Zero-pad spectrum



% Plot zero-padded spectrum
% -------------------------

figure(3);
f = fs*(n_zp/(N+N_zp));
stem(f,abs(X_zp));
xlabel('Frequency (Hz)');
ylabel('Spectral magnitude');
str = ['Spectrum padded by ',num2str(N_zp),' to length ',num2str(N+N_zp)];
title(str);
axis([min(f)-1 max(f)+1 min(abs(X_zp))-0.2 max(abs(X_zp))+0.2])
grid('on');



% Plot IDFT of padded spectrum
% ----------------------------

input("Hit <return> to continue ...");

figure(4);
x_zp = real(ifft(X_zp));
stem(n_zp,x_zp);
xlabel('Time (samples)');
ylabel('Amplitude');
str = ['IDFT of spectrum padded by ',num2str(N_zp),' to length ',num2str(N+N_zp)];
title(str);
axis([min(n_zp)-N_zp/10 max(n_zp)+N_zp/10 min(x_zp)-0.02 max(x_zp)+0.02])
grid('on');




