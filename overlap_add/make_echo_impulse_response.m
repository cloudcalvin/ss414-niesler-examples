% Demonstration script: create an impulse response signal for the convolution demonstration.
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2014.

echoDelay       = 7500;
nEchoes         = 8;
echoAttenuation = 0.9;

fs              = 44100;
B               = 16;

h = zeros(nEchoes*echoDelay,1);
h(1) = 1;
a = echoAttenuation;
for i=1:nEchoes
 h(i*echoDelay) = a;
 a = a * echoAttenuation;
end

% Save to file
wavwrite(h/max(abs(h)),fs,B,'echo-impulse-response.wav')

% Done.

