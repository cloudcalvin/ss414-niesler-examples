% Demonstration script: Direct implementation of convolution
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read speech audio from file
%[x,fs,B]=wavread("where_were_you-male.wav");
[x,fs,B]=wavread('yesterday_lin16_44kHz_mono.wav');


% Read impulse response from file
[h,fs,B]=wavread('echo-impulse-response.wav');

% Pad x with zeros: output will have as many samples as input
x_zp = [x ; zeros(length(h),1)];

% Get start time
start_time = floor(time);

% Convolve h and x_zp:
y = filter(h,[1],x_zp);


% Print elapsed time
end_time = floor(time);
elapsed_time = end_time - start_time;
str = ['Elapsed time: ',num2str(elapsed_time),' seconds.'];
disp(str);

% Save to file
wavwrite(y/max(abs(y)),fs,B,'result_direct.wav')

% Done.

