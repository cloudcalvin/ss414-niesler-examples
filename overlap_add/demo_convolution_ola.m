% Demonstration script: Direct implementation of convolution
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read speech audio from file
%[x,fs,B]=wavread("where_were_you-male.wav");
[x,fs,B]=wavread('yesterday_lin16_44kHz_mono.wav');

% Read impulse response from file
[h,fs,B]=wavread('echo-impulse-response.wav');

N = 2^16;       % Choose FFT length
P = length(h);  % Length of impulse response
L = N-P+1;      % Calculate block length

y = zeros(length(h)+length(x)+L,1);

% Pad h with zeros
h_zp = [h ; zeros(N-length(h),1)];

% Get FFT of h[n]:
H_zp = fft(h_zp);

nBlocks = floor(length(x)/L);

% Print progress
str = ['Processing ',num2str(nBlocks),' blocks each of length ',num2str(L)];
disp(str);

% Get start time
start_time = floor(time);

for (i = 0:nBlocks)
  iStart = i*L + 1;
  iEnd   = iStart + L - 1;

  % Process last block correctly 
  if (iEnd > length(x))
   iEnd = length(x);
  end

  % Calculate convolution using FFT
  x_L     = x(iStart:iEnd);                    % Get next block from x[n]
  x_L_zp  = [x_L ; zeros(N-length(x_L),1)];    % Zero-pad to length N
  X_L_zp  = fft(x_L_zp);                       % Calculate FFT
  Y_L_zp  = X_L_zp .* H_zp;                    % Multiply with FFT of zero-padded impulse response
  y_L_zp = real(ifft(Y_L_zp));                 % Get inverse FFT

  % Overlap and add
  y(iStart:iStart+length(y_L_zp)-1) = y(iStart:iStart+length(y_L_zp)-1) + y_L_zp;
end

% Print elapsed time
end_time = floor(time);
elapsed_time = end_time - start_time;
str = ['Elapsed time: ',num2str(elapsed_time),' seconds.'];
disp(str);

% Save to file
wavwrite(y/max(abs(y)),fs,B,'result_ola.wav')

% Done.

