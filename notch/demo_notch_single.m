% Remove 100Hz interference from corrpupted speech signal using notch filter
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

srcfn = 'speech-with-100Hz.wav';

% Read signal from file
[x,fs,B] = wavread(srcfn);

% Notch parameters
r  = 0.99;  % Notch pole radius
f0 = 100;   % Notch frequency (Hz)

% Transfer function calculation
fw0 = f0/fs;     % Notch frequency (cycles/sample)
w0  = 2*pi*fw0;  % Notch frequency (rad/sample)
b   = [1 -2*cos(w0)   1];
a   = [1 -2*r*cos(w0) r*r];

% Plot notch frequency response
[H,W]=freqz(b,a,10240,'whole');
f = ((0:length(W)-1)*fs)/length(W);
plot(f,20*log10(abs(H)));
xlabel('Frequency (Hz)')
ylabel('Gain (dB)');
title('Notch frequeny response');
grid("on");
axis([0 1000 -30 2]);

% Filter signal with resonator:
y = filter(b,a,x);

% Save filtered signal to WAV file
wavwrite(y/(40*mean(abs(y))),fs,B,'filtered_signal.wav');

