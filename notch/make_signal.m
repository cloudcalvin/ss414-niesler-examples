% Generate speech with 100Hz interference
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

srcfn = 'speech-female.wav'

% Read audio from file
[x,fs,B] = wavread(srcfn);

fe = 100;         % Frequency of interference
Ae = 0.2;         % Amplitude of interference (5 -> -85dB SNR, 0.2-> -20dB)
N  = length(x);   % Number of samples in audio
fw = fe/fs;                     % Freq in rad/sample
e  = Ae*sin(2*pi*fw*(0:N-1));   % Interference

s = x + e';

SNR = 10*log(var(x)) - 10*log((Ae*Ae)/2)

% Save to WAV file
wavwrite(s/max(abs(s)),fs,16,'speech-with-100Hz.wav');

