% Demonstration script: Frequency response using the DFT/FFT
% Case 2: all-pole system
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 200-2014.

N = 16;              % Number of points at which to evaluate the frequency response
hi = [1 0.99 0.49];  % Impulse response of INVERSE system


% Zero-pad impulse response to length N
hi_zp = [hi zeros(1, N-length(hi))];   

% Take N-point FFT
Hi = fft(hi_zp);

% Calculate reciprocal at each point
H = ones(1,length(Hi))./Hi;

% Plot magnitude of frequency response
figure(1);
stem((0:N-1)/N,abs(H),"r");
hold("on");
plot((0:N-1)/N,abs(H),"b--");
hold("off");
xlabel("Frequency (cycles/sample)");
ylabel("Magnitude of frequency response");
title("Frequency response for all-pole system");
legend("off");

% Plot the poles on the zplane
figure(2);
zplane([1],hi);
title('Poles of all-pole filter');
xlabel('Real(z)')
ylabel('Imaginary(z)')

