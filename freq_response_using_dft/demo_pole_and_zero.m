% Demonstration script: Frequency response using the DFT/FFT
% Case 3: system bith both poles and zeros
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

N = 16;               % Number of points at which to evaluate the frequency response
hb = [1 -1.27 0.81];  % Impulse response of all-zero subsystem 
ha = [1  0.99 0.49];  % Impulse response of INVERSE all-pole subsystem

% Zero-pad impulse responses to length N
ha_zp = [ha zeros(1, N-length(ha))];   
hb_zp = [hb zeros(1, N-length(hb))];   

% Take N-point FFTs
Ha = fft(ha_zp);
Hb = fft(hb_zp);

% Determine quotient at each discete frequency
H = Hb./Ha;

% Plot magnitude of frequency response
figure(1);
stem((0:N-1)/N,abs(H),"r");
hold("on");
plot((0:N-1)/N,abs(H),"b--");
hold("off");
xlabel("Frequency (cycles/sample)");
ylabel("Magnitude of frequency response");
title("Frequency response for pole-zero system");
legend("off");

% Plot the poles and zeros on the zplane
figure(2);
zplane(ha,hb);
title('Poles of pole-zero filter');
xlabel('Real(z)')
ylabel('Imaginary(z)')

