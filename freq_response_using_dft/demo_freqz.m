% Demonstration script: Frequency response using the DFT/FFT
% Case 4: using freqz
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

N = 16;               % Number of points at which to evaluate the frequency response
hb = [1 -1.27 0.81];  % Impulse response of all-zero subsystem
ha = [1  0.99 0.49];  % Impulse response of INVERSE all-pole subsystem

% Get frequency response at N points atound unit circle
H = freqz(hb,ha,N,"whole");

% Plot magnitude of frequency response
figure(1);
stem((0:N-1)/N,abs(H),"r");
hold("on");
plot((0:N-1)/N,abs(H),"b--");
hold("off");
xlabel("Frequency (cycles/sample)");
ylabel("Magnitude of frequency response");
title("Frequency response for pole-zero system");
legend("off");

