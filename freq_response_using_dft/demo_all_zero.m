% Demonstration script: Frequency response using the DFT/FFT
% Case 1: all-zero system
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

N = 16;              % Number of points at which to evaluate the frequency response
h = [1 -1.27 0.81];  % Impulse response


% Zero-pad impulse response to length N
h_zp = [h zeros(1, N-length(h))];   

% Take N-point FFT
H = fft(h_zp);

% Plot magnitude of frequency response
figure(1);
stem((0:N-1)/N,abs(H),"r");
hold("on");
plot((0:N-1)/N,abs(H),"b--");
hold("off");
xlabel("Frequency (cycles/sample)");
ylabel("Magnitude of frequency response");
title("Frequency response for all-zero system");
legend("off");

% Plot the zeros on the zplane
figure(2);
zplane(h,[1]);
title('Zeros of all-zero filter');
xlabel('Real(z)')
ylabel('Imaginary(z)')

