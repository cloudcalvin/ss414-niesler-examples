% Demonstration script: Generate spectrogram for nonstationary signal analysis
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

clear;

% Parameters

framelength  = 32;
frameskip    = 32;
sourcefile   = 'siren.wav'
%sourcefile   = 'dtmf_example_16bit_4kHz.wav';
%sourcefile   = 'dress-start.wav';
%sourcefile   = 'where_were_you-male.wav'


% Load data from file
[x,fs,B]=wavread(sourcefile);

% Divide up into frames
rawframes = getframes(x,framelength,frameskip);

[dummy,nframes] = size(rawframes);

% Apply data window to each frame
winframes = windowframes(rawframes,hamming(framelength));

% Determine FFT of each windowed frame
fftframes = fftframes(winframes);

% Plot as image
colormap(ocean(256));
imagesc(log(abs(fftframes)),[-20 max(max(log(abs(fftframes))))])

% saveimage("siren.ps",I,"ps");
% t = 0:nframes-1;
% f = ((0:framelength-1)/framelength)*fs;
% mesh(t, f, abs(fftframes));


