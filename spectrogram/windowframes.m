% Apply window function to sequence of frames
%
% windowedframearray = windowframes(framearray,windowvec)
%
% The elements of windowvec contain the window function
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

function wF = windowframes(framearray,windowvec)

[framelength,nFrames] = size(framearray);
windowlength          = length(windowvec);

wF = 0;

if (framelength != windowlength) 
 disp('ERROR: window length does not correspond to frame length');
 return 
endif

% Prepare output matrix
wF  = zeros(framelength,nFrames);


for i = 1:nFrames
 wF(:,i) = framearray(:,i) .* windowvec;
end
 
