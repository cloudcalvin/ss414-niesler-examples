% Apply FFT function to sequence of frames
%
% fftframearray = fftframes(framearray)
%
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

function fftF = fftframes(framearray)

[framelength,nFrames] = size(framearray);

% Prepare output matrix
fftF  = zeros(framelength,nFrames);

for i = 1:nFrames
 fftF(:,i) = fft(framearray(:,i));
end
 
