% Demonstration script: Generate sinusoid with sinusoidally oscillating frequency
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

fs = 44100;  % Sample frequency (Hz)
N  = 44100;  % Number of samples to generate 
f1 = 8000;   % Mean frequency (Hz)
f2 = 5000;   % Frequency deviation from mean (Hz)
f3 = 4;      % Frequency of frequency deviation (Hz)

n     = 0:N-1;  
theta = 2*pi*(f1/fs)*n - (f2/f3)*cos(2*pi*(f3/fs)*n);
x     = sin(theta);  

wavwrite(x,fs,16,"siren.wav");


