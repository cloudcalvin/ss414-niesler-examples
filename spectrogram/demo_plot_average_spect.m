% Demonstration script: Plot avearge magnitude spectrum of signal.
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.


% Choose source signal here

sourcefile   = 'siren.wav'
%sourcefile   = 'dtmf_example_16bit_4kHz.wav';
%sourcefile   = 'dress-start.wav';
%sourcefile   = 'where_were_you-male.wav'


% Load data from file
[x,fs,B]=wavread(sourcefile);

% Detemine number of samples in data
N = length(x);

% Apply Hamming window
xw = x.*hamming(N);

% Get spectrum
Xw = fft(xw);

% Plot magnitude
f = linspace(0,fs,N);
plot(f,abs(Xw));
xlabel('Frequency (Hz)');
ylabel('Spectral magnitude');
title('Average spectrum of a nonstationary signal');
