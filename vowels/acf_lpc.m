% Brute-force solution to autocorrelation-based LPC equations
% (Uses matrix inversion instead of Levinson-Durbin recursion)
% 
% Usage: a = acf_lpc(x,P)
% 
% where x is the data and P the LPC order.
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

function a = acf_lpc(x,P)

r = zeros(P+1,1);
n = length(x);

% Get autocorrelation values
for i = 0:P
 r(i+1) = sum(x(1:n-i).*x(1+i:n));
end

% Set up normal equations
Rm = toeplitz(r(1:P));
Rv = r(2:P+1);

% Solve normal equations and return
a = inv(Rm)*Rv;
a = [1 ; -a];

