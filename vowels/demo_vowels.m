% Demonstration script: Modelling vowel sounds with all-pole filters
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

infile = 'real_a.wav';   % File to read audio for analysis
nPoles = 0;              % Order of all-pole model 
fsynth = 100;            % Frequency to synthesise at (Hz)

% Read audio from file
[x,fs,B] = wavread(infile);

% Remove DC, if any
x = x - mean(x);

% Plot true spectrum
figure(1);
f = fs*((0:length(x)-1)/length(x));
X = log10(abs(fft(x).*hamming(length(x))));
plot(f,X,'b');
axis([min(f) max(f) min(X(2:length(x))) max(X)]);
xlabel('Frequency (Hz)');
ylabel('Log spectral magnitude');
title('Spectum of natural sound');

% Stop if no LP modelling is to occur.
if (nPoles == 0)
 return;
endif

% Get LPC coefficients
lpcvec = acf_lpc(x,nPoles);

% Get residuals
e = filter(lpcvec,[1],x);

% Determine period of synthetic excitation  
to = round(fs/fsynth);
nGen = length(x);   % Samples to generate

% Make impulse train
s = zeros(1,nGen);
s(1) = 1;
for i = 1:floor(nGen/to)
 s(i*to) = 1;
end

% LPF to remove some of the harshness
[b_lpf,a_lpf] = butter(1, 0.25);
s = filter(b_lpf,a_lpf,s);

% Remove DC, if any
s = s - mean(s);

% Generate synthetic vowel
synth_x = filter([1],lpcvec,s);

% Save to file
wavwrite(synth_x'/max(abs(synth_x)),fs,B,'synth_x.wav');

% Overlay synthesised (LP) spectrum over true spectrum
figure(1);
hold('on');
[H,W] = freqz([1 0],lpcvec,1024,'whole');
f = fs*((0:length(W)-1)/length(W));
plot(f,log10(abs(H)),'r')
hold('off');
legend('off');
title('Spectra of natural and synthetic sounds');

% Plot the poles on the zplane
figure(2);
zplane([1],lpcvec');
title('Poles and zeros of all-pole filter');
xlabel('Real(z)')
ylabel('Imaginary(z)')
