% Demonstration script: Quantisation of continuous-valued signals
% Step 2: quantise signal to desired number of bits
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read audio from file
[ref,fs,B] = wavread('yesterday_lin16_44kHz_mono.wav');

% Amplitudes read into "ref" lie between -1 and +1
% Hence at 16 bits, quantised with interval 1/(0.5 * 2^16) = 0.000030517

N = 12             % N is number of bits to quantise to: (min 1, max 16)
F = (2^(16-N));   % Quantisation factor, 1 for 16-bit, 2 for 15-bit, 4 for 14-bit etc.

int_ref   = round(ref*32768);      % Convert to 16-bit signed integer
trunc_ref = F*(round(int_ref/F));  % Quantised signal
error_ref = int_ref - trunc_ref;   % Error signal


% Calculate signal to quantisation noise ratio (SQNR)
% ---------------------------------------------------

var_sig = var(int_ref(:,1));       % Variance of signal
var_err = var(error_ref(:,1));     % Variance of error

sqnr = 10*log10(var_sig/var_err)   % Calculate SQNR


% Write out quantised waveform and quantisation error signal to file
% ------------------------------------------------------------------

% Scale signal to fall neatly within 16-bit range
SignalGain = (32700)/max(abs(trunc_ref));
ErrorGain  = (32700)/max(abs(error_ref));  
ErrorGain  = ErrorGain / 8;                % Else it's a bit loud

trunc_out = SignalGain * trunc_ref;
error_out = ErrorGain * error_ref;

trunc_fn = ['trunc_',num2str(N),'bit_out.wav'];
error_fn = ['error_',num2str(N),'bit_out.wav'];

wavwrite(trunc_out/32768,fs,B,trunc_fn);
wavwrite(error_out/32768,fs,B,error_fn);


% Plot excerpt from quantised signal
% ----------------------------------

figure(3);
plot(trunc_ref(11000:17999,1));
xlabel('Samples')
ylabel('Amplitude (16-bit integer)')
str = ['Excerpt from audio waveform when quantising to ', num2str(N),' bits'];
title(str);
grid('on')
legend('off')


% Plot excerpt from quantisation error signal
% -------------------------------------------

figure(4);
plot(error_ref(12000:13999,1));
xlabel('Samples')
ylabel('Amplitude (16-bit integer)')
str = ['Excerpt from error waveform when quantising to ', num2str(N),' bits'];
title(str);
grid('on');
legend('off')


% Plot histogram of error signal
% -------------------------------

% Because the calculated error is not between the real (analogue) signal
% and the quantised signal, but between an already-quantised signal and
% a more-roughly-quantised signal, the PDF consists of a sequence of
% peaks, since only a finite number of differences are possible between two
% quantised signals.

figure(5);
R = 0.6;      % x-axis range in multiples of delta
nbins = 70;   % bins in histogram
bins = [-R:(2*R)/nbins:R];
h = hist(error_ref/F,bins);
plot(bins,(h*nbins)/(2*R*sum(h)))
xlabel('Error amplitude/Delta');
ylabel('Probability');
str = ['Distribution of error amplitudes when quantising to ', num2str(N),' bits'];
title(str);
grid('on');
legend('off')
str = ['error_histogram_', num2str(N),'bit.ps'];
print(str,"-dpsc")


