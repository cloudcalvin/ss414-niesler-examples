% Demonstration script: Quantisation of continuous-valued signals
% Step 1: show original waveform and it's sample distribution
% Systems & Signals 414, Thomas Niesler, Stellenbosch University 2007-2014.

% Read audio from file
[ref,fs,B] = wavread('yesterday_lin16_44kHz_mono.wav');

% Amplitudes read into "ref" lie between -1 and +1
% Hence at 16 bits, quantised with interval 1/(0.5 * 2^16) = 0.000030517

int_ref   = round(ref*32768);      % Convert to 16-bit signed integer


% Plot excerpt from unquantised signal
% ------------------------------------

figure(1);
plot(int_ref(11000:17999,1));
xlabel('Samples')
ylabel('Amplitude (16-bit integer)')
title('Excerpt from original 16-bit audio waveform')
grid('on')
legend('off')

% Plot histogram of unquantised signal amplitudes
% -----------------------------------------------

figure(2);
R = 20000;    % x-axis range in multiples of delta
nbins = 60;   % bins in histogram
bins = [-R:(2*R)/nbins:R];
h = hist(int_ref,bins);
plot(bins,(nbins*h)/(2*R*sum(h)))
xlabel('Sample values')
ylabel('Probability of sample value')
title('Distribution of sample values for audio waveform')
grid('on');
legend('off')
print("original_sample_histogram.ps","-dpsc")

